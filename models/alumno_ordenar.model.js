const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const EjercicioAlumno = function(alumno_ordenar) {};

//
EjercicioAlumno.getEjercicioAlumnos = result => {
    sql.query(`SELECT * FROM alumno_ordenar WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

EjercicioAlumno.addEjercicioAlumno = (c, result) => {
    sql.query(`INSERT INTO alumno_ordenar(idalumno,idordenar,puntos,puntos_obtenidos,tiempo_respuesta,frase,respuesta,idnivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`, [c.idalumno, c.idordenar, c.puntos, c.puntos_obtenidos, c.tiempo_respuesta, c.frase, c.respuesta, c.idnivel, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear ejercicio alumno: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}


EjercicioAlumno.updateEjercicioAlumno = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE alumno_ordenar SET idalumno=?,idordenar=?,puntos=?,puntos_obtenidos=?,tiempo_respuesta=?,frase=?,respuesta=?,idnivel=?,fecha_actualizo=?,deleted=? WHERE idalumno_ordenar=?`, [c.idalumno, c.idordenar, c.puntos, c.puntos_obtenidos, c.tiempo_respuesta, c.frase, c.respuesta, c.idnivel, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("ejercicio alumno actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}

//query por alumno
EjercicioAlumno.getEjerciciosAlumno = (id, result) => {
    sql.query(`SELECT * FROM alumno_ordenar WHERE idalumno=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};


module.exports = EjercicioAlumno;