const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Ordenar = function(ordenar) {};

Ordenar.getOrdenar = result => {
    sql.query(`SELECT * FROM ordenar_palabras WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

Ordenar.addOrdenar = (c, result) => {
    sql.query(`INSERT INTO ordenar_palabras(frase,audio,puntos,idnivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?)`, [c.frase, c.audio, c.puntos, c.idnivel, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear ejercicio : ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}

Ordenar.updateOrdenar = (id, c, result) => {
    sql.query(` UPDATE ordenar_palabras SET frase=?,audio=?,puntos=?,idnivel=?,fecha_actualizo=?,deleted=? WHERE idordenar=?`, [c.frase, c.audio, c.puntos, c.idnivel, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("ejercicio actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}



module.exports = Ordenar;