const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Formar = function(formar) {};

Formar.getFormar = result => {
    sql.query(`SELECT * FROM formar_oraciones WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

Formar.addFormar = (c, result) => {
    sql.query(`INSERT INTO formar_oraciones(frase,puntos,idnivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?)`, [c.frase, c.puntos, c.idnivel, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear ejercicio : ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}

Formar.updateFormar = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE formar_oraciones SET frase=?,puntos=?,idnivel=?,fecha_actualizo=?,deleted=? WHERE idformar=?`, [c.frase, c.puntos, c.idnivel, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("ejercicio actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}



module.exports = Formar;