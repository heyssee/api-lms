const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Completar = function(completar) {};

//consulta con join a alumno y la info del alumno
Completar.getCompletar = result => {
    sql.query(`SELECT * FROM completar_oraciones WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

Completar.addCompletar = (c, result) => {
    sql.query(`INSERT INTO completar_oraciones(frase,frase1,frase2,puntos,idnivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?)`, [c.frase, c.frase1, c.frase2, c.puntos, c.idnivel, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear ejercicio: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}


Completar.updateCompletar = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE completar_oraciones SET frase=?,frase1=?,frase2=?,puntos=?,idnivel=?,fecha_actualizo=?,deleted=? WHERE idcompletar=?`, [c.frase, c.frase1, c.frase2, c.puntos, c.idnivel, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("ejercicio actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}


module.exports = Completar;