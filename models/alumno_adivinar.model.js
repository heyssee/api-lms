const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const EjercicioAlumno = function(alumno_adivina) {};

//consulta con join a alumno y la info del alumno
EjercicioAlumno.getEjercicioAlumnos = result => {
    sql.query(`SELECT * FROM alumno_adivinar WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

EjercicioAlumno.addEjercicioAlumno = (c, result) => {
    sql.query(`INSERT INTO alumno_adivinar(idalumno,idadivinar,puntos,puntos_obtenidos,tiempo_respuesta,respuesta1,respuesta2,respuesta3 ,presente,pasado,participio,imagen,idnivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [c.idalumno, c.idadivinar, c.puntos, c.puntos_obtenidos, c.tiempo_respuesta, c.respuesta1, c.respuesta2, c.respuesta3, c.presente, c.pasado, c.participio, c.imagen, c.idnivel, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear ejercicio alumno: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}


EjercicioAlumno.updateEjercicioAlumno = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE alumno_adivinar SET idalumno=?,idadivinar=?,puntos=?,puntos_obtenidos=?,tiempo_respuesta=?,respuesta1=?,respuesta2=?,respuesta3=?,presente=?,pasado=?,participio=?,imagen=?,idnivel=?,fecha_actualizo=?,deleted=? WHERE idalumno_adivinar=?`, [c.idalumno, c.idadivinar, c.puntos, c.puntos_obtenidos, c.tiempo_respuesta, c.respuesta1, c.respuesta2, c.respuesta3, c.presente, c.pasado, c.participio, c.imagen, c.idnivel, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("ejercicio alumno actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}

//consulta para obtener el ejercicios por alumno
EjercicioAlumno.getEjerciciosAlumno = (id, result) => {
    sql.query(`SELECT * FROM alumno_adivinar WHERE idalumno=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

//consulta para obtener el ejercicios por alumno y nivel 
EjercicioAlumno.getAdivinarNivelAlumno = (c, result) => {
    sql.query(`SELECT * FROM adivinar_palabras WHERE deleted = 0 AND idnivel=?`, [c.idnivel], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }

        var ejercicios = []
        for (const i in res) {
            var datos = {
                idadivinar: res[i].idadivinar,
                presente: res[i].presente,
                pasado: res[i].pasado,
                participio: res[i].participio,
                imagen: res[i].imagen,
                puntos: res[i].puntos,
                idnivel: res[i].idnivel,
                idalumno: c.idalumno,
                puntos_obtenidos: 0
            }
            ejercicios.push(datos)
        }
        console.log("ejercicio pt1:", ejercicios)

        sql.query(`SELECT * FROM alumno_adivinar WHERE idalumno=? AND deleted = 0`, [c.idalumno], (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in ejercicios) {
                for (const j in res) {
                    if (ejercicios[i].idadivinar === res[j].idadivinar) {
                        ejercicios[i].puntos_obtenidos = res[j].puntos_obtenidos;
                    }
                }
            }

            var show = []
            for (const i in ejercicios) {
                if (ejercicios[i].puntos_obtenidos === 0) {
                    show.push(ejercicios[i])
                }
            }


            console.log("ejercicios al: ", show);
            result(null, show);
        });
    });
};


module.exports = EjercicioAlumno;