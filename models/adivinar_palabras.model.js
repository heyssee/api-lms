const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Adivinar = function(adivina) {};

Adivinar.getAdivinar = result => {
    sql.query(`SELECT * FROM adivinar_palabras WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

Adivinar.addAdivinar = (c, result) => {
    sql.query(`INSERT INTO adivinar_palabras(presente,pasado,participio,imagen,puntos,idnivel,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?)`, [c.presente, c.pasado, c.participio, c.imagen, c.puntos, c.idnivel, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear ejercicio : ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}

Adivinar.updateAdivinar = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE adivinar_palabras SET presente=?,pasado=?,participio=?,imagen=?,puntos=?,idnivel=?,fecha_actualizo=?,deleted=? WHERE idadivinar=?`, [c.presente, c.pasado, c.participio, c.imagen, c.puntos, c.idnivel, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("ejercicio actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}

Adivinar.getAdivinarNivel = (id, result) => {
    sql.query(`SELECT * FROM adivinar_palabras WHERE deleted = 0 AND idnivel=?`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

module.exports = Adivinar;