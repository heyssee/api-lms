const Formar = require("../models/formar_oraciones.model.js");


exports.getFormar = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Formar.getFormar((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};

exports.addFormar = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Formar.addFormar(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el ejercicio"
            })
        else res.status(200).send(data);
    })
};

exports.updateFormar = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Formar.updateFormar(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el ejercicio con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el ejercicio con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El ejercicio se ha actualizado correctamente.` })
        }
    })
}