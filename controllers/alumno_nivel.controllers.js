const AlumnoNivel = require("../models/alumno_nivel.model.js");


exports.getAlumnoNivel = (req, res) => {

    AlumnoNivel.getAlumnoNivel((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios del alumno"
            });
        else res.send(data);
    });
};


exports.addAlumnoNivel = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    AlumnoNivel.addAlumnoNivel(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la nota"
            })
        else res.status(200).send({ message: `Se ha creado correctamente` });
    })
};

exports.updateAlumnoNivel = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    AlumnoNivel.updateAlumnoNivel(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el alumno nivel con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el alumno nivel con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El alumno nivel se ha actualizado correctamente.` })
        }
    })
}

exports.getAlumnoNiveles = (req, res) => {
    AlumnoNivel.getAlumnoNiveles(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los alumnos nivel"
            });
        else res.send(data);
    });
};


exports.getAlumnosNivel = (req, res) => {
    AlumnoNivel.getAlumnosNivel(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los alumnos nivel"
            });
        else res.send(data);
    });
};