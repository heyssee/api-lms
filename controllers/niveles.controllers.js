const Niveles = require("../models/niveles.model.js");


exports.getNiveles = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Niveles.getNiveles((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los niveles"
            });
        else res.send(data);
    });
};

exports.addNivel = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Niveles.addNivel(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la nota"
            })
        else res.status(200).send({ message: `El nivel se ha creado correctamente` });
    })
};

exports.updateNivel = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Niveles.updateNivel(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el nivel con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el nivel con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El nivel se ha actualizado correctamente.` })
        }
    })
}

exports.getNivelAdivinarAlumno = (req, res) => {
    Niveles.getNivelAdivinarAlumno(req.params.id, (err, data) => {

        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};