module.exports = app => {
    const alumno_formar = require('../controllers/alumno_formar.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/alumno_formar.all", alumno_formar.getEjercicioAlumnos); // OBTENER TODOS LOS EJERCICIOS
    app.post("/alumno_formar.add", alumno_formar.addEjercicioAlumno); //AGREGAR 
    app.put("/alumno_formar.update/:id", alumno_formar.updateEjercicioAlumno); //actualizar 
    app.get("/alumno_formar.alumno/:id", alumno_formar.getEjercicioAlumno); // OBTENER TODOS LOS EJERCICIOS DE UN ALUMNO

};