module.exports = app => {
    const archivos = require('../controllers/archivos.controllers') // --> ADDED THIS
    app.post("/imagenes", archivos.addImagen);

    app.post("/audios", archivos.addAudio);
};