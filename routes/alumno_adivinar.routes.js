module.exports = app => {
    const alumno_adivinar = require('../controllers/alumno_adivinar.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/alumno_adivinar.all", alumno_adivinar.getEjercicioAlumnos); // OBTENER TODOS LOS EJERCICIOS
    app.post("/alumno_adivinar.add", alumno_adivinar.addEjercicioAlumno); //AGREGAR 
    app.put("/alumno_adivinar.update/:id", alumno_adivinar.updateEjercicioAlumno); //actualizar 
    app.get("/alumno_adivinar.alumno/:id", alumno_adivinar.getEjerciciosAlumno); // OBTENER TODOS LOS EJERCICIOS DE UN ALUMNO
    app.post("/alumno_adivinar.nivel", alumno_adivinar.getAdivinarNivelAlumno); //Alumno por nivel 
};