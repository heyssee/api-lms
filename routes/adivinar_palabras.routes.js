module.exports = app => {
    const adivinar_palabras = require('../controllers/adivinar_palabras.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/adivinar_palabras.all", adivinar_palabras.getAdivinar); // OBTENER
    app.get("/adivinar_palabras.nivel/:id", adivinar_palabras.getAdivinarNivel); // OBTENER
    app.post("/adivinar_palabras.add", adivinar_palabras.addAdivinar); //AGREGAR 
    app.put("/adivinar_palabras.update/:id", adivinar_palabras.updateAdivinar); //actualizar 

};