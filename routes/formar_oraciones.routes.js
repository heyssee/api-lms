module.exports = app => {
    const formar_oraciones = require('../controllers/formar_oraciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/formar_oraciones.all", formar_oraciones.getFormar); // OBTENER
    app.post("/formar_oraciones.add", formar_oraciones.addFormar); //AGREGAR 
    app.put("/formar_oraciones.update/:id", formar_oraciones.updateFormar); //actualizar 

};