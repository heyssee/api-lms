module.exports = app => {
    const alumno_completar = require('../controllers/alumno_completar.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/alumno_completar.all", alumno_completar.getEjercicioAlumnos); // OBTENER TODOS LOS EJERCICIOS
    app.post("/alumno_completar.add", alumno_completar.addEjercicioAlumno); //AGREGAR 
    app.put("/alumno_completar.update/:id", alumno_completar.updateEjercicioAlumno); //actualizar 
    app.get("/alumno_completar.alumno/:id", alumno_completar.getEjercicioAlumno); // OBTENER TODOS LOS EJERCICIOS DE UN ALUMNO

};