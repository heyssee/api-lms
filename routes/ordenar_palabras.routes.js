module.exports = app => {
    const ordenar_palabras = require('../controllers/ordenar_palabras.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/ordenar_palabras.all", ordenar_palabras.getOrdenar); // OBTENER
    app.post("/ordenar_palabras.add", ordenar_palabras.addOrdenar); //AGREGAR 
    app.put("/ordenar_palabras.update/:id", ordenar_palabras.updateOrdenar); //actualizar 

};